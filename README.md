## Install Rancher via Docker

### 1. Install Docker Engine
```
yum -y install docker && systemctl enable docker --now
```

### 2. Install Rancher as Docker Container
```
sudo docker run --name rancher_container -d --restart=unless-stopped -p 80:80 -p 443:443 --privileged rancher/rancher:v2.5-head
```

### 3. Access Rancher UI via https://IP

<br />
<br />
<br />

## Install Rancher within K8S (using Helm)

### 1. Ensure that Helm is installed
```
helm version
```

### 2. Add Repository for Rancher
```
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
```
```
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
```

### 3. Create Namespace for Rancher
```
kubectl create namespace cattle-system
```

### 4. Choose SSL configuration
- Rancher Generated Certificates (Default)
- Let’s Encrypt
- Certificates from Files

### 5. Install cert-manager (in case of certs issued by Rancher's generated CA)
```
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.4/cert-manager.crds.yaml

# Create the namespace for cert-manager
kubectl create namespace cert-manager

# Add the Jetstack Helm repository
helm repo add jetstack https://charts.jetstack.io

# Update your local Helm chart repository cache
helm repo update

# Install the cert-manager Helm chart
helm install cert-manager jetstack/cert-manager --namespace cert-manager

kubectl get pods --namespace cert-manager
```

### 6. Install Rancher (Rancher-generated certs)
```
# stable:
helm install rancher rancher-stable/rancher --namespace cattle-system --set hostname=rancher.opensovereigncloud.com

# wait:
kubectl -n cattle-system rollout status deploy/rancher

# verify:
kubectl -n cattle-system get deploy rancher
```

More: https://rancher.com/docs/rancher/v2.5/en/installation/install-rancher-on-k8s/

### 7. Accessin the Rancher's UI
Since Rancher is deployed as the ClusterIP type of Service, we need to work around it in order to access it remotely by creating new Service but of the type Node Port.

```
[root@ip-10-0-2-101 ~]# cat rancher-svc-np.yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    meta.helm.sh/release-name: rancher
    meta.helm.sh/release-namespace: cattle-system
  labels:
    app: rancher
  name: rancher-svc-np
  namespace: cattle-system
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
    nodePort: 30080
  - name: https-internal
    port: 443
    protocol: TCP
    targetPort: 444
    nodePort: 30443
  selector:
    app: rancher
  type: NodePort
```

As soon as the Service is deployed you can access Rancher's GUI via:

https://PUBLIC_IP:30443
