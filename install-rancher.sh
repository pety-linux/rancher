#!/bin/bash

## Add Helm repository for Rancher
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable

## Create namespace for Rancher
kubectl create namespace cattle-system

## Cert-manager installation
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.4/cert-manager.crds.yaml
# Create the namespace for cert-manager
kubectl create namespace cert-manager
# Add the Jetstack Helm repository
helm repo add jetstack https://charts.jetstack.io
# Update your local Helm chart repository cache
helm repo update
# Install the cert-manager Helm chart
helm install cert-manager jetstack/cert-manager --namespace cert-manager

## Rancher installation
# stable release
helm install rancher rancher-stable/rancher --namespace cattle-system --set hostname=rancher.opensovereigncloud.com
# wait
kubectl -n cattle-system rollout status deploy/rancher
# verify:
kubectl -n cattle-system get deploy rancher

## Rancher Ingress Resource
kubectl apply -f ingress-rancher.yaml
